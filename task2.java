/*kode ini memeriksa apakah seseorang memenuhi syarat untuk mendonor darah berdasarkan usia dan berat badannya. Jika usia orang tersebut lebih dari atau sama dengan 18 tahun dan berat badannya lebih dari 50, maka akan muncul tulisan "Anda berhak mendonorkan darah". Jika tidak, akan muncul tulisan "Anda tidak berhak mendonorkan darah" atau "Usia harus lebih dari 18 tahun" jika usia orang tersebut kurang dari 18 tahun. */

public class task2 {
    public static void main(String[] args) {
        int age = 25; // inisialisasi variabel usia
        int weight = 48; // inisialisasi variabel berat badan
        if (age >= 18) { // memeriksa apakah usia lebih dari atau sama dengan 18
            if (weight > 50) { // memeriksa apakah berat badan lebih dari 50
                System.out.println("You are eligible to donate blood"); // menampilkan output string
            } else { // jika berat badan kurang dari 50
                System.out.println("You are not eligible to donate blood"); // menampilkan outpit string
            }
        } else { // jika usia kurang dari 18
            System.out.println("Age must be greater than 18"); // menampilkan output string
        }
    }
}
