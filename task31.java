/*kode ini membuat ArrayList string dan menambahkan empat buah ke dalamnya. Ia kemudian menggunakan iterator untuk mengulangi daftar dan mencetak setiap buah ke konsol. */

import java.util.ArrayList;
import java.util.*;

public class task31 {
    public static void main(String args[]) {
        ArrayList<String> list = new ArrayList<String>(); // membuat arraylist string
        list.add("Mango"); // menambahkan string "Mango" ke dalam arraylist dengan method "add"
        list.add("Apple"); // menambahkan string "Apple" ke dalam arraylist dengan method "add"
        list.add("Banana"); // menambahkan string "Banana" ke dalam arraylist dengan method "add"
        list.add("Grapes"); // menambahkan string "Grapes" ke dalam arraylist dengan method "add"
        Iterator itr = list.iterator(); // membuat iterator dari arraylist list
        while (itr.hasNext()) { // perulangan untuk mencetak setiap buah dalam arraylist dengan method "hasNext"
                                // yang digunakan untuk mengecek apakah ada setiap buah dalam arraylist dengan
                                // iterator "itr"
            System.out.println(itr.next()); // mencetak setiap buah dalam arraylist dengan method "next" yang digunakan
                                            // untuk mengakses setiap buah dalam arraylist dengan iterator "itr"
        }
    }

}

/* Perbedaan akan dijelaskan di class task32 */