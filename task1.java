/*kode ini merupakan perulangan do- while dengan statement kondisional.

Program dimulai dengan menginisialisasi variabel i dengan nilai 1. Kemudian, program memasuki perulangan do- while yang akan mengeksekusi kode di dalam perulangan setidaknya satu kali.

Di dalam perulanga  kode akan memeriksa apakah i sama dengan 5. Jika ya, ia menambah i sebanyak 1 dan keluar dari perulangan. Jika tidak, ia akan mencetak nilai i dan menambahnya sebesar 1.

Perulangan berlanjut samp  i kurang dari atau sama dengan 10. */
public class task1 {

    public static void main(String[] args) throws Exception {
        int i = 1; // inisialisas variabel i dengan nilai 1
        do { // ok perulangan
            if (i == 5) { // jika i sama dengan 5
                i++; // tambahkan 1 ke variabel i, increment variabel i
                break; // keluar dari perulangan
            }
            System.out.println(i); // cetak i
            //
            i++; // tambahkan 1 ke variabel i, increment variabel i
        } while (i <= 10); // perulangan hingga i kurang dari atau sama dengan 10
    }
}

// output yang keluar yaitu, 1, 2, 3, 4 karena ketika i = 5, perulangan akan
// berhenti (ditandai dengan break)
