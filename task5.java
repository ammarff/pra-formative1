import java.util.HashMap;
import java.util.Map;

public class task5 {

    public static void main(String[] args) {
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "Mango");
        map.put(2, "Apple");
        map.put(3, "Banana");
        map.put(4, "Grapes");
        map.put("pisang", "Pisang"); // menambahkan key dan value baru, disini ada error karena data 'key' yang
                                     // diinputkan bukan integer (sesuai dengan inisialisasi)

        System.out.println("Iterating Hashmap...");
        for (Map.Entry m : map.entrySet()) {
            System.out.println(m.getKey() + " " + m.getValue());
        }
    }
}
