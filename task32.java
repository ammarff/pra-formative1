import java.util.ArrayList;

public class task32 {
    public static void main(String args[]) {
        ArrayList<String> list = new ArrayList<String>(); // membuat arraylist string
        list.add("Mango"); // menambahkan string "Mango" ke dalam arraylist dengan method "add"
        list.add("Apple"); // menambahkan string "Apple" ke dalam arraylist dengan method "add"
        list.add("Banana"); // menambahkan string "Banana" ke dalam arraylist dengan method "add"
        list.add("Grapes"); // menambahkan string "Grapes" ke dalam arraylist dengan method "add"
        System.out.println(list); // mencetak arraylist
    }
}

/*
 * Berikut adalah perbedaan antara task31 dan task32
 * 
 * task31: Membuat ArrayList dengan tipe data String. Menambahkan beberapa
 * string ke dalam ArrayList menggunakan metode add. Menggunakan Iterator untuk
 * melintasi setiap elemen dalam ArrayList dan mencetaknya menggunakan metode
 * next.
 * task32: Juga membuat ArrayList dengan tipe data String. Menambahkan beberapa
 * string ke dalam ArrayList menggunakan metode add. Mencetak seluruh ArrayList
 * menggunakan System.out.println.
 * Jadi, perbedaan utama antara task31 dan task32 adalah cara mereka mencetak
 * elemen-elemen dalam ArrayList. task31 menggunakan Iterator untuk melintasi
 * dan mencetak setiap elemen satu per satu, sedangkan task32 mencetak seluruh
 * ArrayList secara langsung.
 */